# opennic-resolve

Thoroughly tested and in daily use on Archlinux & Debian stable.  
This version requires name resolution to be already working when executed.  
OpenNIC's geoip sorting is [known][ki] to be unreliable, therefore this script attempts to fetch a larger list, then sort it by reliability percentage, and optionally filter by countries.
It is capable of writing different formats (systemd, resolvconf) to multiple files.  
Systemd unit files (service and timer) are provided.

## Installation

For ArchLinux compatible distros, opennic-resolve ~~is~~ will soon be [in the AUR][A].

Users of other distros can run `./install.sh -h` for installation options.

After installation, execute the service with

    systemctl start opennic-resolve.service

And start the timer with

    systemctl enable --now opennic-resolve.timer

## --help output

    This script will fetch a current list of DNS servers from api.opennicproject.org,
    filter them by countries if desired, sort by percentage, and write the result
    to files readable by various resolvers.
    It will then restart some systemd services if they are active. If the systemctl
    executable is not found in PATH, the script will gracefully exit instead.
    
    The script takes no command line options (except "-h" which produces this
    output, everything else is discarded), but searches for a config file in
    "$HOME/.config/opennic-resolve/opennic-resolve.conf"
    "/etc/opennic-resolve/opennic-resolve.conf"
    "$0.conf" i.e. the full path to the script + .conf
    "$PWD/opennic-resolve.conf" i.e. in the present working directory
    Whichever is encountered first will be the only one to parse.
    
    Defaults and possible configuration:
    
    countries=  Provide a comma-separated list of 2-letter country codes.
                Nameservers from other countries won't be added to the file.
                Empty by default - no filtering by country.
    
    max=        Maximum number of entries to write to a file. Default: 5
                Must be between 2 and 50, although it appears that the API rarely
                returns more than 20 results anyhow.
    
    min=        Minimum number of entries to write to a file. Default: 2
                If fewer nameservers are found no file will be overwritten.
    
    timeout=    Timeout in s for data fetching from api.opennicproject.org. default: 10
    
    out=        Comma-separated list of actions. Possible values:
                resolvconf
                    each line is either a comment starting with "#" or
                    "nameserver " followed by the IP of the nameserver
                resolved
                    starts with "[Resolve]", after which each line is either a
                    comment starting with "#" or "DNS=" followed by the IP
                    of the nameserver
                networkd
                    starts with "[Network]", after which each line is either a
                    comment starting with "#" or "DNS=" followed by the IP
                    of the nameserver
                Overrides the default, which is "resolvconf,resolved"
    
    resolvconf= Write to this file instead of /etc/resolv.conf
    networkd=   Write networkd syntax to this file (no default)
    resolved=   Write to this file instead of /etc/systemd/resolved.conf.d/dns.conf
    
    services=   Comma-separated list of systemd services to restart, if active.
                Overrides the default, which is:
                "systemd-networkd.service,systemd-resolved.service"
    
    More information:
    https://notabug.org/ohnonot/opennic https://framagit.org/ohnonot/opennic

Keep in mind that various software can override nameservers defined in this way, e.g.:  
NetworkManager, netctl, dnsmasq, openresolv, resolvconf, systemd-resolvconf ...  
So you might want to disable these.

If you rely only on `dhcpcd` to connect to the internet:

## dhcpcd

It typically relies on `/etc/resolv.conf` for name resolution.
No service restart is required in this case, but make sure that nothing overwrites
this file:

- Add the option `nohook resolv.conf` to `/etc/dhcpcd.conf`.
- Make sure `/etc/dhcpcd.conf` does not define any nameservers.
- Make sure dhcpcd triggers no other software that might overwrite `/etc/resolv.conf`.

Further reading: https://wiki.archlinux.org/index.php/Dhcpcd#/etc/resolv.conf

## Dependencies

- `jq` to parse json
- `curl`
- coreutils

[A]: https://aur.archlinux.org/packages/opennic-resolve
[ki]: https://wiki.opennic.org/api/geoip#known_issues
